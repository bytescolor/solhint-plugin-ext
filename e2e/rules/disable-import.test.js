const linter = require('solhint')
const code = `
    import "lib.sol";
    contract A {}
`
describe('Linter - using-import' , () => {
    test('should error when using-import == false', ()=> {
        let report = linter.processStr(code, {
            plugins: ['ext'],
            rules: { 'disable-import': 'error' }
        });
        console.log(report)
        expect(report).not.toBeNull()
    })
})