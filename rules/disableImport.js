
class DisableImport{
    constructor(reporter, config) {
        this.ruleId = 'disable-import'
        this.reporter = reporter
        this.config = config
    }
    SourceUnit(node) {
        console.log(this.ruleId, this.config)
        for (let i = 0; node.children && i < node.children.length; i += 1) {
            const curItem = node.children[i]
            if (curItem.type === 'ImportDirective') {
                this.error(curItem, 'Import directive is disable.')
            }
        }
    }
    error(ctx, message, fix) {
        this.addReport('error', ctx, message, fix)
    }
    addReport(type, ctx, message, fix) {
        this.reporter[type](ctx, this.ruleId, message, this.meta.fixable ? fix : null)
    }
}

module.exports = DisableImport